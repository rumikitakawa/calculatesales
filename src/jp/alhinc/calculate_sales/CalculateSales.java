package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	//商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	//商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "ファイルのフォーマットが不正です";
	private static final String FILE_NOT_SERIAL_NUMBER = "売上ファイル名が連番になっていません";
	private static final String FILE_OVER_DIGIT = "合計金額が10桁を超えました";
	private static final String FILE_INVALID_BRANCH_CODE = "の支店コードが不正です";
	private static final String FILE_INVALID_COMMODITY_CODE = "の商品コードが不正です";
	private static final String FILE_OVER_LINE =  "のフォーマットが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {

		//コマンドライン引数が渡されているか
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();

		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();

		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales= new HashMap<>();

		//支店定義ファイル名の正規表現
		String branchFormat = "[0-9]{3}";
		//商品定義ファイル名の正規表現
		String commodityFormat = "^[A-Za-z0-9]+{8}$";

		String branchFileExist = "支店定義";
		String commodityFileExist = "商品定義";

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales,branchFileExist,branchFormat)) {
			return;
		}

		//商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales,commodityFileExist,commodityFormat)) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)

		//コマンドライン引数で指定されたフォルダ
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFile = new ArrayList<>();

		//売上ファイルの抽出
		for (int i = 0; i < files.length; i++) {
			String name = files[i].getName();
			//ファイルである、かつ数字8桁のrcdファイルであるか
			if(files[i].isFile() && name.matches("[0-9]{8}.+rcd$")){
				//売上ファイルのみrcdFileリストに入れる
				rcdFile.add(files[i]);
				return;
			}
		}

		//rcdFileのリストを昇順にソートする
		Collections.sort(rcdFile);

		//リストが連番か
		for (int i = 0; i < rcdFile.size() - 1; i++) {
			int former = Integer.parseInt(rcdFile.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFile.get(i + 1).getName().substring(0,8));
			//連番ではない場合
			if((latter - former) != 1) {
				System.out.println(FILE_NOT_SERIAL_NUMBER);
				return;
			}
		}
		String line;
		BufferedReader br = null;

		//rcdFileの中身を読み込む
		for (int i = 0; i < rcdFile.size(); i++) {
			try {
				FileReader fr = new FileReader(rcdFile.get(i));
				br = new BufferedReader(fr);

				//リストの作成
				List<String> fileData = new ArrayList<>();

				//fileDataリストに支店コード、商品コード、売上金額を入れる
				while((line = br.readLine()) != null) {
					fileData.add(line);
				}

				//売上ファイルの中身が3行か
				if(fileData.size() != 3) {
					System.out.println(rcdFile.get(i).getName() + FILE_OVER_LINE);
					return;
				}

				//支店コードが支店定義ファイルに存在するか
				if(! branchSales.containsKey(fileData.get(0))) {
					System.out.println(rcdFile.get(i).getName() + FILE_INVALID_BRANCH_CODE);
					return;
				}

				//商品コードが商品定義ファイルに存在するか
				if(! commoditySales.containsKey(fileData.get(1))) {
					System.out.println(rcdFile.get(i).getName() + FILE_INVALID_COMMODITY_CODE);
					return;
				}

				//売上金額が数字であるか
				if(! fileData.get(1).matches ("^[0-9]*$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				//String型からLong型へ変換
				long fileSale = Long.parseLong(fileData.get(2));

				//売上金額の加算
				Long saleAmount = branchSales.get(fileData.get(0)) + fileSale;
				Long commodityAmount = commoditySales.get(fileData.get(0)) + fileSale;

				//合計金額が10桁以上ではないか
				if(saleAmount >= 10000000000L || commodityAmount >= 10000000000L) {
					System.out.println(FILE_OVER_DIGIT);
					return;
				}

				//加算したものを支店コードと売上金額を保持するMapに保持させる
				branchSales.put(fileData.get(0), saleAmount);
				//加算したものを商品コードと売上金額を保持するMapに保持させる
				commoditySales.put(fileData.get(1),commodityAmount);

			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}

			// 支店別集計ファイル書き込み処理
			if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
				return;
			}
			//商品別集計ファイル書き込み処理
			if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
				return;
			}
		}
	}
		/**
		 * 支店定義ファイル読み込み処理
		 *
		 * @param フォルダパス
		 * @param ファイル名
		 * @param 支店コードと支店名を保持するMap
		 * @param 支店コードと売上金額を保持するMap
		 * @return 読み込み可否
		 */
		private static boolean readFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales, String fileExist,String fileFormat) {
			BufferedReader br = null;

			try {
				File file = new File(path, fileName);
				if(!file.exists()) {
					System.out.println(fileExist + FILE_NOT_EXIST);
					return false;
				}
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);

				String line;
				// 一行ずつ読み込む
				while((line = br.readLine()) != null) {
				 // ※ここの読み込み処理を変更してください。(処理内容1-2)
				 String[] items = line.split(",");

				//支店定義ファイル及び商品定義ファイルのフォーマットが正しいか
				if((items.length != 2) || (! items[0].matches(fileFormat))) {
					System.out.println(fileExist + FILE_INVALID_FORMAT);
					return false;
				}

				names.put(items[0],items[1]);
				sales.put(items[0],(long) 0);
				}

			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return false;

			} finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return false;
					}
				}
			}
			return true;
		}

		/**
		 * 支店別集計ファイル書き込み処理
		 *
		 * @param フォルダパス
		 * @param ファイル名
		 * @param 支店コードと支店名を保持するMap
		 * @param 支店コードと売上金額を保持するMap
		 * @return 書き込み可否
		 */
		private static boolean writeFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {
			// ※ここに書き込み処理を作成してください。(処理内容3-1)
			BufferedWriter bw = null;

			try {
				//新しいファイルを作成
				File newFile = new File(path,fileName);
				FileWriter fw = new FileWriter(newFile);
				bw = new BufferedWriter(fw);

				for(String key:names.keySet()) {
					String Name = names.get(key);
					Long Sale = sales.get(key);
					bw.write(key + "," + names + "," + sales);
					//改行する
					bw.newLine();
				}
			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return false;
			} finally {
				// ファイルを開いている場合
				if(bw != null) {
					try {
						// ファイルを閉じる
						bw.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return false;
					}
				}
			}
			return true;
		}
	}
